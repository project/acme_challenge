<?php

namespace Drupal\acme_challenge;

use Drupal\Core\Config\ConfigManagerInterface;

/**
 * Class ChallengeService.
 */
class ChallengeService {

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * Constructs a new ChallengeService object.
   */
  public function __construct(ConfigManagerInterface $config_manager) {
    $this->configManager = $config_manager;
  }

  /**
   * Get the stored value for a key. null if empty.
   *
   * @param string $key
   *   The key for which the value should looked up.
   *
   * @return string|null
   *   The value for the key
   */
  public function getValueForKey($key) {
    /** @var Drupal\Core\Config\ImmutableConfig $configuration */
    $configuration = $this->configManager->getConfigFactory()
      ->get('acme_challenge.acmechallengeconfiguration');

    $keyValueConfigString = $configuration->get('acme_challenge_key_value_setting');

    if (FALSE === empty($keyValueConfigString)) {
      $keyValuesString = preg_split("/\r\n|\n|\r/", $keyValueConfigString);
      foreach ($keyValuesString as $keyValueString) {
        $keyValue = explode('|', $keyValueString);
        if (2 === count($keyValue)) {
          if ($key === $keyValue[0]) {
            return $keyValue[1];
          }
        }
      }
    }

    return NULL;
  }

}
