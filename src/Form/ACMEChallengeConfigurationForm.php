<?php

namespace Drupal\acme_challenge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ACMEChallengeConfigurationForm.
 */
class ACMEChallengeConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'acme_challenge.acmechallengeconfiguration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acme_challenge_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('acme_challenge.acmechallengeconfiguration');
    $form['acme_challenge_key_value_setting'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Key Value Settings'),
      '#description' => $this->t('Add the ACME challenge keys (name of the challenged file) and value (content of the challenged file). Each line is one challenge, seperated by "|" . <br />E.g. BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A|BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A.lyEr19AK0ZqcLPXK91ynoddGTrWs_u6zUAPWI9E9cmY'),
      '#default_value' => $config->get('acme_challenge_key_value_setting'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('acme_challenge.acmechallengeconfiguration')
      ->set('acme_challenge_key_value_setting', $form_state->getValue('acme_challenge_key_value_setting'))
      ->save();
  }

}
