<?php

namespace Drupal\acme_challenge\Controller;

use Drupal\acme_challenge\ChallengeService;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ACMEChallengeController.
 */
class ACMEChallengeController extends ControllerBase {

  /**
   * The ChallengeService needed to perform the magic.
   *
   * @var \Drupal\acme_challenge\ChallengeService
   */
  protected $challengeService;

  /**
   * ACMEChallengeController constructor.
   *
   * @param \Drupal\acme_challenge\ChallengeService $challengeService
   *   The ChallengeService needed to perform the magic.
   */
  public function __construct(ChallengeService $challengeService) {
    $this->challengeService = $challengeService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('acme_challenge.challenge')
    );
  }

  /**
   * Perform ACME Challenge.
   *
   * @return string
   *   Return value for challenged key.
   */
  public function performAcmeChallenge($key) {
    $value = $this->challengeService->getValueForKey($key);
    return new Response($value, NULL !== $value ? 200 : 404);
  }

}
