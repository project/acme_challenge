# ACME Challenge

If using Let's encrypt or a free online tool to generate free SSL certificates, 
a ACME Challenge needs to be performed in order to prove the ownership of the domain.

In most cases this can be either done by creating an additional DNS entry - or by
coping a file with given title and content to the location 
*/.well-known/acme-challenge/{KEY_OF_THE_FILE}*.

## INTRODUCTION`
This module allows to to "add" such a file by configuration instead of uploading it
manually to this position.

## REQUIREMENTS
There are no special requirements. A clean Drupal 8 instance is just perfect
start for **ACME Challenge**!

## INSTALLATION
1. Install the **ACME Challenge** module
2. Go to the configuration and add the generated and given Keys and Values.

## CONFIGURATION
There is only one textarea. Each line is one challenge (e.g. for multiple domains).

E.g. You retrieve the following file to prove the ownership:

Filename (Key): 

*BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A*

Content (Value): 

*BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A.lyEr19AK0ZqcLPXK91ynoddGTrWs_u6zUAPWI9E9cmY*

Add the Key concatenated with a | and the Value in the textarea:

````
BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A.|BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A.lyEr19AK0ZqcLPXK91ynoddGTrWs_u6zUAPWI9E9cmY

`````

If everything works, opening 
*/.well-known/acme-challenge/BT4J0ksoodADJzIe3SW7YJXKojbVsquSTTXvqvfsZ5A*
should return the content.
